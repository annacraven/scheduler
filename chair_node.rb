require './node.rb'
require './time.rb'

class ChairNode < Node
  attr_accessor :chair, :time

  def initialize(chair, time)
    super(chair.id)
    @chair = chair
    @time = time
    @fragmentation = false
  end

  def mutually_exclusive(other)
    return false if @chair.day != other.chair.day
    return true if @time == other.time
    if @chair.location != other.chair.location
      # check if self comes directly before other
      return true if time_add(@time, 15) == other.time
      # check if other comes directly before self
      return true if time_add(other.time, 15) == @time
    end
    false
  end

  def assign(e_node)
    @assigned_employee = e_node
  end

  def note_fragmentation
    @fragmentation = true
  end

  def fragmentation?
    @fragmentation
  end

  def to_s
    return "#{@fragmentation ? "*" : ""}chair node: #{@number}, location: #{@chair.location}, day: #{@chair.day} time: #{@time}, assigned to: #{@assigned_employee&.number}"
  end

  def print
    puts self.to_s
  end

  def <=>(other)
    # return nil if self.chair.id != other.chair.id
    return 0 if self == other
    return -1 if self < other
    return 1
  end


  def ==(other)
    false
  end

  def <(other)
    if other.chair.id == self.chair.id
      return self.number < other.number
    end
    self.chair.id < other.chair.id
  end
end