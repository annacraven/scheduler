# https://www.rubyguides.com/2017/05/graph-theory-in-ruby/
require 'rgl/adjacency'
require 'rgl/dot'

require './helpers.rb'
require './mocks.rb'
require './employee_node.rb'
require './chair_node.rb'
require './prescheduling.rb'
require './schedule.rb'

class UnweightedAlgo

  def initialize(chairs, employees, max_hours=19, max_shift_length = 1)
    @max_hours = max_hours
    @max_shift_length = max_shift_length
    generate_graphs(chairs, employees)
  end

  # Step 1: Generate a graph to represent the problem
  def generate_graphs(chairs, employees)
    # these two graphs represent the pseudobipartite graph described in the report
    @chair_exc_graph = RGL::AdjacencyGraph.new
    @avail_graph = RGL::DirectedAdjacencyGraph.new
    @chair_adj_graph = RGL::DirectedAdjacencyGraph.new # nodes are c_nodes

    # for every employee, plus a null employee, create an E node
    @e_nodes = {} # number => Enode
    employees.each do |e|
      e_node = EmployeeNode.new(e)
      @e_nodes[e_node.number] = e_node
    end

    null_e = EmployeeNode.new(nil)
    @e_nodes[null_e.number] = null_e

    # for every one of those chairs, break up into single-person 15 minute subchairs, and create a C node for each subchair
    # while creating subchairs, create the chairs_adj_graph
    @c_nodes = {} # number => Cnode
    chairs.each do |c|
      time = c.start_time
      previous_level = []
      while time < c.end_time
        curr_level = []
        c.min_people.times do 
          c_node = ChairNode.new(c, time)
          @c_nodes[c_node.number] = c_node
          # add and edge in the chair_adj_graph from each node in the previous level to c
          previous_level.each do |prev_chair|
            @chair_adj_graph.add_edge prev_chair, c_node
          end
          curr_level << c_node
        end
        time = time_add(time, 15)
        previous_level = curr_level
      end
    end

    # for every C node c, add an edge to any other C node c' iff c and c' are mutually exclusive
    @c_nodes.each_value do |c1|
      @c_nodes.each_value do |c2|
        next if c1 == c2
        @chair_exc_graph.add_edge c1.number, c2.number if c1.mutually_exclusive(c2)
      end
    end

    # for every E node e, add an edge to every C node that e is available and allowed to work
    @e_nodes.each_value do |e|
      @c_nodes.each_value do |c|
        if e.employee.nil?
          @avail_graph.add_edge c.number, e.number 
          next
        end
        @avail_graph.add_edge c.number, e.number if e.available(c)
      end
    end
  end

  # Step 2: Order the C nodes to perform prescheduling
  def preschedule
    @c_nodes = preschedule(C, avail_graph)
  end

  # Step 3: Traverse the graph to generate a schedule
  def run
    @schedule = Schedule.new(@chair_adj_graph)
    # for every node c in C
    @c_nodes.each_value do |c|
      # randomly select an edge between c and a node e in E
      e_num = rand(@avail_graph.out_degree(c.number))
      selected = @avail_graph.each_adjacent(c.number).lazy.drop(e_num).first
      e = @e_nodes[selected]
      # e = select_e(c)
      # in schedule, assign e to work c
      @schedule.assign(c, e)
      # if e is the null employee or chair has no mutually exclusive chairs, continue
      next if e.null? or not @chair_exc_graph.has_vertex?(c.number)
      # for every edge between c and a node c2 in C
      @chair_exc_graph.each_adjacent(c.number) do |c2|
        # if there is an edge between c2 and e, remove it
        @avail_graph.remove_edge(c2, e.number)
      end
    end
    return @schedule
  end

  # @return e_node
  def select_e(c_node)
    e_num = rand(@avail_graph.out_degree(c.number))
    selected = @avail_graph.each_adjacent(c.number).lazy.drop(e_num).first
    return @e_nodes[selected]
  end

  def assign(chair, employee)
    @schedule.assign(ChairNode.new(chair), EmployeeNode.new(employee))
  end

  def print_graphs
    s = RGL::DirectedAdjacencyGraph.new()
    @schedule.each do |c, e|
      s.add_edge(c, e)
    end
    # print the graphs
    @chair_exc_graph.print_dotted_on
    @avail_graph.print_dotted_on
    @chair_adj_graph.print_dotted_on
    s.print_dotted_on
  end

  def print_assignments_by_employee
    @e_nodes.each_value do |e_node|
      puts "Assignments for Employee #{e_node.number}"
      puts e_node.get_assignments
    end
  end

  def output_result(output_graphs=false)
    # return schedule
    if output_graphs
      print_graphs
    else
      @schedule.print
      print_assignments_by_employee
    end
  end
end

