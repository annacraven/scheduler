
# given an x value, return the corresponding y value for the line formed by (x0, y0) and (x1, y1)
# point slope form: y − y1 = m(x − x1)
def point_slope(point0, point1, x)
  m = (point1[1] - point0[1]) / (point1[0] - point0[0])
  y = m*(x - point1[0]) + point1[1]
  return y
end