require './valid.rb'
require './fitness.rb'
require './unweighted_algo.rb'
require './weighted_algo.rb'
require './mocks.rb'

db = MockDB.new
chairs = db.get_chairs
employees = db.get_employees

puts "Unweighted Algorithm"
unweighted = UnweightedAlgo.new(db.get_chairs, db.get_employees)
schedule = unweighted.run
find_fragmentations(schedule)
unweighted.output_result

print "Checking validity: "
puts valid?(schedule)

puts "Checking fitness:"
fitnesses = fitness(chairs, employees, schedule)
puts "\tchairs filled: #{fitnesses[0]}"
puts "\temployees near target hours: #{fitnesses[1]}"
puts "\temployees within hours range: #{fitnesses[2]}"
puts "\temployees at preferred location: #{fitnesses[3]}"
puts "\tfragmentation: #{fitnesses[4]}"

puts "Weighted Algorithm"
Node.reset
weighted = WeightedAlgo.new(db.get_chairs, db.get_employees)
schedule2 = weighted.run
find_fragmentations(schedule2)
weighted.output_result

print "Checking validity: "
puts valid?(schedule2)

puts "Checking fitness:"
fitnesses = fitness(chairs, employees, schedule2)
puts "\tchairs filled: #{fitnesses[0]}"
puts "\temployees near target hours: #{fitnesses[1]}"
puts "\temployees within hours range: #{fitnesses[2]}"
puts "\temployees at preferred location: #{fitnesses[3]}"
puts "\tfragmentation: #{fitnesses[4]}"