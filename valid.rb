require 'time.rb'

'''
Let us define a valid schedule as one where none of these conditions are true:
a) an employee is scheduled to work outside of their availability.
b) an employee is scheduled to work at a location where they are not permitted to work.
c) an employee is scheduled for overlapping chairs, or adjacent chairs in different locations.
d) (*ignored for now) a non-lead employee is scheduled without a lead in a location that requires
a lead
'''

# @input [Hash<ChairNode => EmployeeNode>] schedule
# @return [Boolen], true iff schedule is valid
def valid?(schedule)
  schedule.each do |c_node, e_node|
    next if e_node.null?
    return false if (not check_permission(c_node, e_node) or\
      not check_availability(c_node, e_node) or\
      not check_no_overlap(c_node, e_node, schedule) or\
      not check_no_adjacent(c_node, e_node, schedule))
  end
  true
end

private 

# return true if e is allowed to work c
# TODO logic to be implemented in scheduler project
def check_permission(c, e)
  true
end

# return true if e is available to work c
# in scheduler project, user.covers_timeslot?(slot)
def check_availability(c,e)
  e.employee.availabilities.each do |a|
    return true if a.day == c.chair.day and c.time.between?(a.start_time, a.end_time)
  end
  puts "invalid due to availability constraint"
  false
end

# return true if in schedule, e has no chairs that overlap with c
def check_no_overlap(c,e,schedule)
  schedule.each do |c_node, e_node|
    next if e_node.employee != e.employee or c_node = c or c.chair.day != c_node.chair.day
    if c.time == c_node.time
      puts "overlap: #{c.time}:#{c.number}, #{c_node.time}:#{c_node.number}, #{e.number}"
      puts "invalid due to overlap constraint"
      return false
    end
    next if c.chair.location == c_node.chair.location
  end
  true
end

def check_no_adjacent(c,e,schedule)
  schedule.each do |c_node, e_node|
    next if e_node.employee != e.employee or c.chair.day != c_node.chair.day or c.chair.location == c_node.chair.location
    if time_add(c.time,15) == c_node.time or time_add(c_node.time,15) == c.time
      puts "invalid due to adjacency constraint"
      return false
    end
  end
  true
end