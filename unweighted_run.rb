require './unweighted_algo.rb'
require './mocks.rb'

db = MockDB.new
unweighted = UnweightedAlgo.new(db.get_chairs, db.get_employees)
schedule = unweighted.run
unweighted.output_result(true)