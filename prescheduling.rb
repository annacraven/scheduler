# return an order of chairs such that any chairs which only have one available employee (besides the null employee) will precede the other
def preschedule(c_hash, bipartite_graph)
  pre, oth = c_hash.each_with_object([{}, {}]) do |(num, c), (precedents, others)|
    degree_one?(c, bipartite_graph) ? precedents[num] = c : others[num] = c
  end
  pre.merge(oth)
end

def degree_one?(node, graph)
  graph.has_vertex?(node.number) and graph.out_degree(node.number) == 2
end