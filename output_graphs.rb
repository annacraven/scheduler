system "ruby unweighted_run.rb > ./output/unweighted_output.txt"

chair_exc_graph = File.open("./output/chair_exc_graph.txt", "w")
bipartite_graph = File.open('./output/bipartite_graph.txt', "w")
chair_adj_graph = File.open('./output/chair_adj_graph.txt', "w")
schedule_graph = File.open('./output/schedule_graph.txt', "w")
files = [chair_exc_graph, bipartite_graph, chair_adj_graph, schedule_graph]
text = ""
file_index = 0

File.readlines('./output/unweighted_output.txt').each do |line|
  text << line
  if line.include?("}")
    files[file_index].write(text.lstrip)
    file_index += 1
    text = ""
  end
end
files.each {|f| f.close}

system "dot -Tpdf ./output/chair_exc_graph.txt > ./output/chair_exc_graph.pdf"
system "dot -Tpdf ./output/bipartite_graph.txt > ./output/bipartite_graph.pdf"
system "dot -Tpdf ./output/chair_adj_graph.txt > ./output/chair_adj_graph.pdf"
system "dot -Tpdf ./output/schedule_graph.txt > ./output/schedule_graph.pdf"

system "rm ./output/chair_exc_graph.txt"
system "rm ./output/bipartite_graph.txt"
system "rm ./output/chair_adj_graph.txt"
system "rm ./output/schedule_graph.txt"

system "ruby weighted_run.rb > ./output/weighted_output.txt"

chair_exc_graph = File.open("./output/w_chair_exc_graph.txt", "w")
bipartite_graph = File.open('./output/w_bipartite_graph.txt', "w")
chair_adj_graph = File.open('./output/w_chair_adj_graph.txt', "w")
schedule_graph = File.open('./output/w_schedule_graph.txt', "w")
files = [chair_exc_graph, bipartite_graph, chair_adj_graph, schedule_graph]
text = ""
file_index = 0

File.readlines('./output/weighted_output.txt').each do |line|
  text << line
  if line.include?("}")
    files[file_index].write(text.lstrip)
    file_index += 1
    text = ""
  end
end
files.each {|f| f.close}

system "dot -Tpdf ./output/w_chair_exc_graph.txt > ./output/w_chair_exc_graph.pdf"
system "dot -Tpdf ./output/w_bipartite_graph.txt > ./output/w_bipartite_graph.pdf"
system "dot -Tpdf ./output/w_chair_adj_graph.txt > ./output/w_chair_adj_graph.pdf"
system "dot -Tpdf ./output/w_schedule_graph.txt > ./output/w_schedule_graph.pdf"

system "rm ./output/w_chair_exc_graph.txt"
system "rm ./output/w_bipartite_graph.txt"
system "rm ./output/w_chair_adj_graph.txt"
system "rm ./output/w_schedule_graph.txt"