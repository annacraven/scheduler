require "./unweighted_algo.rb"
require "./weights.rb"

class WeightedAlgo < UnweightedAlgo
  def run(output_graphs=false)
    initialize_weights
    @schedule = Schedule.new(@chair_adj_graph)
    # for every node c in C
    # (@c_nodes.values.sort_by { |c_node| c_node }) .each do |c|
    @c_nodes.each_value do |c|
      # select the highest weighted edge between c and a node e in E
      max_weight = -1
      max_e_num = -1
      @avail_graph.each_adjacent(c.number) do |e_num|
        weight = @weights.get_weight(c, @e_nodes[e_num])
        if weight >= max_weight
          max_weight = weight
          max_e_num = e_num
        end
      end
      e = @e_nodes[max_e_num]
      # in schedule, assign e to work c
      @schedule.assign(c, e)
      @weights.update_weight(c,e, @schedule)
      # if e is the null employee or chair has no mutually exclusive chairs, continue
      next if e.null? or not @chair_exc_graph.has_vertex?(c.number)
      # for every edge between c and a node c2 in C
      @chair_exc_graph.each_adjacent(c.number) do |c2|
        # if there is an edge between c2 and e, remove it
        @avail_graph.remove_edge(c2, e.number)
      end
    end
    return @schedule
  end

  private

  def initialize_weights
    @weights = Weights.new(@e_nodes, @chair_adj_graph, @max_hours, @max_shift_length)
  end
end