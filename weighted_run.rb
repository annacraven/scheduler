require './weighted_algo.rb'
require './mocks.rb'

db = MockDB.new
weighted = WeightedAlgo.new(db.get_chairs, db.get_employees)
schedule = weighted.run
weighted.output_result(true)