class Weights
  def initialize(e_nodes, chair_adj_graph, max_hours, max_shift_length)
    @chair_adj_graph = chair_adj_graph
    @max_hours = max_hours
    @max_shift_length = max_shift_length
    # edge weights
    @shift_length = {}

    # employee weights
    @assigned_hours = initialize_assigned_hours(e_nodes)
    @availability_rank = calculate_availability_rank(e_nodes)

    # chair_weights

  end

  def initialize_assigned_hours(e_nodes)
    weights = {}
    e_nodes.each_value do |e|
      weights[e] = e.null? ? -1 : 1
    end
    return weights
  end

  # calculate the percentile rank of each employee's total availability
  # Percentile = (number of values below score) ÷ (total number of scores)
  # the weight for the availability rank is 1 - percentile
  def calculate_availability_rank(e_nodes)
    percentiles = {}
    availabilities = {}
    e_nodes.each_value do |e|
      availabilities[e] = e.total_availability
    end
    availabilities = availabilities.sort_by { |e, a| a }.to_h
    availabilities.keys.each_with_index do |e,i|
      percentiles[e] = 1 - (i / availabilities.length.to_f)
    end
    return percentiles
  end

  def update_weight(c_node, e_node, schedule)
    return if e_node.null?
    @assigned_hours[e_node] = e_node.evaluate_assigned_hours(schedule.get_assigned_hours(e_node.employee), @max_hours)

    # for every chair node adjacent to c_node, increase the assignment weight by 10%
    curr_len = @shift_length.fetch([c_node,e_node], 1)
    @chair_adj_graph.each_adjacent(c_node) do |adj|
      @shift_length[[adj, e_node]] = @shift_length.fetch([adj,e_node], 1) + curr_len
    end
  end

  # may weight this sum, or make it an average
  def get_weight(c_node, e_node)
    return -1 if e_node.null?
    get_edge_weight(c_node, e_node) + get_employee_weight(e_node) + get_chair_weight(c_node)
  end

  private
  # return a weighted sum of the various measures
  def get_edge_weight(c_node, e_node)
    return 0 if e_node.null?
    edge = c_node, e_node
    preferred_location_weight(c_node, e_node) + shift_length_weight(c_node, e_node)
  end

  # return a weighted sum of the various measures
  def get_employee_weight(e_node)
    return 0 if e_node.null?
    return @assigned_hours[e_node] + @availability_rank[e_node]
  end

  # return a weighted sum of the various measures
  def get_chair_weight(c_node)
    return 0
  end

  # 1 if chair is at the employee's preferred location
  def preferred_location_weight(c_node, e_node)
    return 1 if c_node.chair.location = e_node.employee.preferred_location
    return 0
  end

  def shift_length_weight(c_node, e_node)
    weight = @shift_length.fetch([c_node, e_node], 0)
    return -1 if weight * 15 / 60.to_f > @max_shift_length
    return weight
  end
end