require './models/employee.rb'
require './models/chair.rb'


class MockDB

  def initialize(num_chairs=5, num_employees=5)
    @employees = Employee.generate(num_employees)
    @chairs = generate_chairs(num_chairs)
  end

  def get_employees
    return @employees
  end

  def get_chairs
    return @chairs
  end

  private

  def generate_chairs(num)
    # arr = Chair.generate(num)
    arr = []
    arr << Chair.new(0,1030,0,1330,2)
    arr << Chair.new(0,1030,1)
    arr << Chair.new(0,1045,1)
    arr << Chair.new(0,1100,2)
    arr << Chair.new(1,1100,2)
    arr << Chair.new(2,1100,2)
    return arr
  end
end

