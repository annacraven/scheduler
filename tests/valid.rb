require './valid.rb'
require './unweighted_algo.rb'
require './mocks.rb'

db = MockDB.new
unweighted = UnweightedAlgo.new(db.get_chairs, db.get_employees)
schedule = unweighted.run

puts valid?(schedule) ? "PASS" : "FAIL"

new_chair = Chair.new(6,1030,0)
unavail_employee = Employee.new
unweighted.assign(new_chair, unavail_employee)
puts ((not valid?(schedule)) ? "PASS" : "FAIL")
