require './node.rb'
require './helpers.rb'

class EmployeeNode < Node
  attr_accessor :employee, :assigned_hours
  def initialize(e)
    super(e&.id)
    @employee = e
    @assignments = []
  end

  def available(chair_node)
    avails = @employee.availabilities
    chair = chair_node.chair
    avails.each do |a|
      next if a.day != chair.day
      return true if chair.start_time >= a.start_time and chair.start_time <= a.end_time
    end
    false
  end

  def allowed(chair)
    # TODO logic to be implemented in scheduler project
    true
  end

  def total_availability
    return 0 if self.null?
    @employee.availabilities.sum {|a| a.length}
  end

  def assign(c_node)
    @assignments << c_node
  end

  def get_assignments
    @assignments.sort_by {|c_node| [c_node.chair.day, c_node.time] }
  end

  def null?
    @employee.nil?
  end

  def number
    self.null? ? "-" : super
  end

  def to_s
    "employee node: #{@number}"
  end

  def inspect
    return self.number
  end

  # returns a value between -1 and 1, indicating how close the employee's assigned hours are to their target hours
  # max_hours is parameter set by the manager, defaults to 19
  def evaluate_assigned_hours(assigned_hours, max_hours)
    return 0 if self.null?
    points = [[0,1], [@employee.min_hours,0.75], [@employee.target_hours,0], [@employee.max_hours,-0.75], [max_hours,-1]]
    curr = 0
    while curr < points.length - 1
      if assigned_hours.between?(points[curr][0], points[curr+1][0])
        return point_slope(points[curr], points[curr+1], assigned_hours)
      end
      curr += 1
    end
  end

end