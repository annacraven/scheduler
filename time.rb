
def time_add(t1, minutes, hours=0)

  hour = (t1 / 100) + hours
  min = t1.remainder(100)
  min += minutes
  if (min >= 60)
    hour = (hour + 1) % 24
    min %= 60
  end
  hour * 100 + min
end

def time_difference(t1, t2)
  h1 = t1 / 100
  h2 = t2 / 100
  hours = t1 - t2
  m1 = t1.remainder(100)
  m2 = t2.remainder(100)
  min = m1 - m2
  return hours + (min / 60.to_f)
end

def random_day
  rand(8)
end

def random_time
  hour = rand(24) * 100
  minute = rand(4) * 15
  hour + minute
end