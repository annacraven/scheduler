
class Schedule
  attr_accessor :chair_adj_graph
  def initialize(chair_adj_graph)
    @assignments = {} # c_node => e_node
    @assigned_hours = {}
    @chair_adj_graph = chair_adj_graph
  end

  def assign(c_node, e_node)
    c_node.assign(e_node)
    e_node.assign(c_node)
    @assignments[c_node] = e_node
    @assigned_hours[e_node.employee] = get_assigned_hours(e_node.employee) + 0.25
  end

  def each
    @assignments.each { |c_node, e_node| yield c_node, e_node if not e_node.null? }  
  end

  def get_assigned_employee(c_node)
    @assignments[c_node]
  end

  def get_assigned_hours(employee)
    return 0 if not @assigned_hours.key?(employee)
    @assigned_hours[employee]
  end

  def size
    @assignments.size
  end

  def print
    @assignments.each do |c, e|
      puts "chair #{c.number}, employee #{e.number}"
    end
  end
end