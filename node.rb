class Node
  @@number_of_nodes = 0
  def initialize(id)
    # id of the object this node represents, either a chair id or a employee id.
    @id = id # note: this is not distinct, because there can be multiple nodes for one chair
    @number = @@number_of_nodes
    @@number_of_nodes +=1
  end

  def self.reset
    @@number_of_nodes = 0
  end

  attr_accessor :number
end