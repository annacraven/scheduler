
# note: these are not 15-minute single person chairs
# higher fitness indicates a better schedule
def fitness(chairs, employees, schedule)
  return [
    chairs_filled(schedule), 
    employees_near_target_hours(employees, schedule), 
    employees_within_hours_range(employees, schedule),
    employees_at_preferred_location(schedule),
    fragmentation_count(schedule, 1, true)
  ]
end

def chairs_filled(schedule)
  total = schedule.size
  filled = 0
  schedule.each do |c_node, e_node|
    filled += 1 if not e_node.null?
  end
  return filled / total.to_f
end

# return average distance from target hours, normalized to between 0 and 1, 1 being all employees at target hours
def employees_near_target_hours(employees, schedule)
  dist = 0
  employees.each do |e|
    dist += (schedule.get_assigned_hours(e) - e.target_hours).abs
  end
  avg = dist / employees.size.to_f
  max_distance = 19*employees.size
  return 1 - (avg / max_distance)
end

def employees_within_hours_range(employees, schedule)
  total = employees.size
  good = 0
  employees.each do |e|
    good += 1 if schedule.get_assigned_hours(e).between?(e.min_hours, e.max_hours)
  end
  return good / total.to_f
end

def employees_at_preferred_location(schedule)
  total = schedule.size
  good = 0
  schedule.each do |c,e|
    good += 1 if c.chair.location == e.employee.preferred_location
  end
  return good / total.to_f
end

# def average_shift_length(schedule)
#   total = 0

# end

def fragmentation_count(schedule, gap_size=1, debug=false)
  fragmentation_num = 0
  schedule.each do |c_node, e_node|
    fragmentation_num += 1 if c_node.fragmentation?
  end
  return 1 - (fragmentation_num / schedule.size.to_f)
end

def find_fragmentations(schedule, gap_size=2, debug=false)
  fragmentations = []
  schedule.each do |c_node, e_node|
    if fragmentation?(schedule, c_node, c_node, gap_size + 1) # +1 to account for the starting node
      fragmentations << c_node.number
      c_node.note_fragmentation
    end
  end
  print "fragmentations: ", fragmentations, "\n" if debug
end

# @return true if there is a fragmentation from the start node, going through the curr node, of length 
def fragmentation?(schedule, start, curr, length)
  employee = schedule.get_assigned_employee(start)
  if length == 1
    # return true if there is an adj node assigned to that employee, false otherwise
    schedule.chair_adj_graph.each_adjacent(curr) do |adj|
      if employee == schedule.get_assigned_employee(adj)
        return true
      end
    end
    return false
  end

  # if any adjacent nodes are assigned to that employee, return false
  schedule.chair_adj_graph.each_adjacent(curr) do |adj|
    if employee == schedule.get_assigned_employee(adj)
      return false
    end
  end

  # for every adj node not assigned to that employee, continue looking for fragmentations
  schedule.chair_adj_graph.each_adjacent(curr) do |adj|
    if employee != schedule.get_assigned_employee(adj)
      return true if fragmentation?(schedule, start, adj, length - 1)
    end
  end
  return false
end