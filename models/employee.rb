require './models/availability.rb'

class Employee
  attr_accessor :id, :availabilities, :target_hours, :min_hours, :max_hours, :preferred_location

  @@number_of_employees = 0
  def initialize
    @id = @@number_of_employees
    @@number_of_employees += 1
    @availabilities = Availability.generate(rand(20))
    @availabilities << Availability.new(0, 0, 2345)
    @target_hours = rand(20)
    @min_hours = @target_hours - rand(@target_hours)
    @max_hours = @target_hours + rand(20 - @target_hours)
    @target_hours = 1
    @min_hours = 0.25
    @max_hours = 3
    @preferred_location = 0
  end

  def print
    puts self.inspect
  end

  def self.generate(num)
    arr = Array.new(num) { self.new }
    e = Employee.new
    e.availabilities << Availability.new(2, 0, 1200)
    arr << e
  end
end