class Availability
  attr_accessor :start_time, :end_time, :day

  @@number_of_avails = 0
  def initialize(day=nil, start_time=nil, end_time=nil)
    @id = @@number_of_avails
    @@number_of_avails += 1
    @start_time = start_time || random_time
    @end_time = end_time || time_add(@start_time, 0, 2)
    @day = day || random_day
  end

  def length
    time_difference(@end_time, @start_time)
  end

  def print
    puts self.inspect
  end

  def self.generate(num)
    return Array.new(num) { self.new }
  end
end