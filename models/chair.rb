
require './time.rb'
NUM_LOCATIONS = 4

class Chair
  @@number_of_chairs = 0
  def initialize(day=random_day, start_time=random_time, location=random_location, end_time=nil, min_people=random_people)
    @id = @@number_of_chairs
    @day = day
    @start_time = start_time
    @end_time = end_time || time_add(start_time, 30)
    @location = location
    @min_people = min_people
    @@number_of_chairs += 1
  end

  attr_accessor :id, :day, :start_time, :end_time, :location, :min_people

  def print
    puts self.inspect
  end

  def self.generate(num)
    return Array.new(num) { self.new(0) }
  end

  private


  def random_location
    return rand(NUM_LOCATIONS)
  end

  def random_people
    return rand(1..3)
  end
end